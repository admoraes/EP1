#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>

using namespace std;

class Imagem{
	private:
		string numeroMagico;
		string comentario;
		int altura;
		int largura;
		int corMaxima;
		int * matrizPixels;

	public:
		Imagem();
		~Imagem();

		void carregarImagem(string caminho);

		void setNumeroMagico(string numeroMagico);
		void setComentario(string comentario);
		void setAltura(int altura);
		void setLargura(int largura);
		void setCorMaxima(int corMaxima);

		string getNumeroMagico();
		string getComentario();
		int getAltura();
		int getLargura();
		int getCorMaxima();

		void setRed(int x, int y, int red);
		void setGreen(int x, int y, int green);
		void setBlue(int x, int y, int blue);

		int getRed(int x, int y);
		int getGreen(int x, int y);
		int getBlue(int x, int y);

};

#endif
