#include "../inc/imagem.hpp"

#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>

using namespace std;

Imagem::Imagem(){}

Imagem::~Imagem(){delete [] matrizPixels;}

void Imagem::carregarImagem(string caminhoImagem){
	ifstream imagem;
	ofstream dadosImagem;

	char pixelImagem;

	string linha;

	imagem.open(caminhoImagem.c_str(), ios::binary);

	dadosImagem.open("./doc/dadosImagem.txt");

	if (imagem.fail()){
		cerr << "Erro ao tentar abrir o arquivo: " << caminhoImagem << endl;
		imagem.clear();
	}
	else{
		while(getline(imagem, linha, '\n')){
      		if(linha[0] != '#'){      
         		for (int i=0; linha[i]!='\0' ; i++ ){
            		if(linha[i] == ' '){
               			linha[i] = '\n';
            		}
         		}
			dadosImagem << linha << endl;
			}
    	}
	dadosImagem.close();

	imagem.close();

	imagem.open("./doc/dadosImagem.txt", ios::binary);

	getline(imagem, linha);
	setNumeroMagico(linha);

	int intImagem;

	imagem >> intImagem;
	setLargura(intImagem);

	imagem >> intImagem;
	setAltura(intImagem);

	imagem >> intImagem;
	setCorMaxima(intImagem);

	matrizPixels = new int [largura*altura*3];

	for(int i=0; i<largura; i++){
		for(int j=0; j<altura; j++){
			imagem.get(pixelImagem);
			setRed(largura, altura, pixelImagem);

			imagem.get(pixelImagem);
			setGreen(largura, altura, pixelImagem);

			imagem.get(pixelImagem);
			setBlue(largura, altura, pixelImagem);
		}
	}

	}


}

string Imagem::getNumeroMagico() {
	return numeroMagico;
}

void Imagem::setNumeroMagico(string numeroMagico) {
  this -> numeroMagico = numeroMagico;
}

string Imagem::getComentario() {
  return comentario;
}

void Imagem::setComentario(string comentario) {
  this -> comentario = comentario;
}

int Imagem::getLargura() {
  return largura;
}

void Imagem::setLargura(int largura) {
  this -> largura = largura;
}

int Imagem::getAltura() {
  return altura;
}

void Imagem::setAltura(int altura) {
  this -> altura = altura;
}

int Imagem::getCorMaxima() {
  return corMaxima;
}

void Imagem::setCorMaxima(int corMaxima) {
  this -> corMaxima = corMaxima;
}

void Imagem::setRed(int x, int y, int red){
	matrizPixels[x+(largura*y)] = red;
}

void Imagem::setGreen(int x, int y, int green){
	matrizPixels[x+(largura*y)+1] = green;
}
void Imagem::setBlue(int x, int y, int blue){
	matrizPixels[x+(largura*y)+2] = blue;
}

int Imagem::getRed(int x, int y){
	return matrizPixels[x+(largura*y)];
}

int Imagem::getGreen(int x, int y){
	return matrizPixels[x+(largura*y)+1];
}

int Imagem::getBlue(int x, int y){
	return matrizPixels[x+(largura*y)+2];
}
