#include "../inc/imagem.hpp"

#include <string>
#include <iostream>

using namespace std;

int main(){
	Imagem imagemInicial;

	string caminhoImagem;

	cout << "Insira o caminho com e o nome da imagem: " << endl;
	cin >> caminhoImagem;

	imagemInicial.carregarImagem(caminhoImagem);

	cout << "Numero magico: " << imagemInicial.getNumeroMagico() << endl;
	cout << "Largura: " << imagemInicial.getLargura() << endl;
	cout << "Altura: " << imagemInicial.getAltura() << endl;
	cout << "Cor Maxima: " << imagemInicial.getCorMaxima() << endl;

	return 0;
}
